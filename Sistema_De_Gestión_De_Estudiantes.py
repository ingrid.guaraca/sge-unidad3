#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"

# Escriba un programa para construir un Sistema de Gestión de Estudiantes simple usando Python que pueda realizar
# las siguientes operaciones sobre la base de datos "academia" construida en las clases de la asignatura:
# Matricular estudiante
# Visualizar datos del estudiante
# Buscar estudiante por id
# Eliminar estudiante por id

import sqlite3

class BD:

    def __init__(self):
        self.conexion = sqlite3.connect('academia.sqlite')
        self.cursor = None
        print("Conexion establecida con éxito!")

    def crear_bd(self):
        sql_estudiantes = """CREATE TABLE ESTUDIANTES (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, 
        Nombre  TEXT UNIQUE, 
        Apellido  TEXT,
        Cédula  INTEGER,
        Email  TEXT, 
        Representante  TEXT)"""
        sql_asignaturas = """ CREATE TABLE ASIGNATURAS (
        id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        Nombre   TEXT UNIQUE
        )"""
        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql_estudiantes)
        self.cursor.execute(sql_asignaturas)
        print("Tablas creadas!")
        self.cursor.close()

    # Matricular estudiante
    def matricular_estudiante(self):
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO ESTUDIANTES (Nombre, Apellido, Cédula, Email, Representante) VALUES (?, ?, ?, ?, ?)",
                                [('Carla', 'Lapo', '0786825620', 'cl@gmail.com', 'Rocio Vega'),
                                 ('Marco', 'Blanco', '0736628243', 'mb@gmail.com', 'Marisol Ramos'),
                                 ('Luciana', 'Celi', '0793648524', 'lc@gmail.com', 'Nancy Mejia'),
                                 ('Katrina', 'Mora', '0752877446', 'km@gmail.com', 'Emilio Roca'),
                                 ('Bruno', 'Jaen', '0730837242', 'bj@gmail.com', 'Cataleya López')])
        self.conexion.commit()
        self.cursor.close()
        print ("Estudiantes matriculados.")

    def ingresar_asignatura(self):
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO ASIGNATURAS (Nombre) VALUES(?)",
                                [('Matemática',),
                                 ('Lengua y Literatura',),
                                 ('Biología',),
                                 ('Química',),
                                 ('Arte',)])
        self.conexion.commit()
        self.cursor.close()
        print ("Asignaturas creadas con éxito.")

    # Visualizar datos del estudiante
    def mostrar_datos(self):
        self.cursor = self.conexion.cursor()
        print("ESTUDIANTES MATRICULADOS")
        sql = """SELECT * FROM ESTUDIANTES"""
        self.cursor.execute(sql)
        matri = self.cursor.fetchall()
        for ma in matri:
            print(ma)
        self.cursor.close()

    # Buscar estudiante por id
    def buscar_estudiante(self):
        print("ESTUDIANTE")
        self.cursor = self.conexion.cursor()
        self.cursor.execute("SELECT * FROM ESTUDIANTES WHERE id=3")
        estudiante = self.cursor.fetchone()
        print(estudiante)
        self.conexion.close()

    # Eliminar estudiante por id
    def eliminar_estudiante(self):
        print("ESTUDIANTES")
        self.cursor = self.conexion.cursor()
        self.cursor.execute("DELETE FROM ESTUDIANTES WHERE id=2")
        print()
        for estudiante in self.cursor.execute("SELECT * FROM ESTUDIANTES"):
            print(estudiante)

if __name__ == "__main__":
    base = BD()
    #base.crear_bd()
    #base.matricular_estudiante()
    #base.ingresar_asignatura()
    #base.mostrar_datos()
    #base.buscar_estudiante()
    #base.eliminar_estudiante()